<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\ServiceAttribute;
use App\Models\Service;
use App\Models\Order;
use DB;

use Illuminate\Support\Facades\Validator;
class HomeController extends Controller
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $services = Service::all();
        $nowDate = date('Y-m-d');
        $todayDelivery   = Order::where('delivery_date', $nowDate)->count();
        $totalPending    = Order::where('delivery_date', '>',$nowDate)
        ->where('record_status', 'Pending')->count();

        $orderDeliveryOver    = Order::where('delivery_date', '<', $nowDate)
        ->where('record_status', 'Pending')->count();

        $orderCancelled    = Order::where('delivery_date', '<', $nowDate)
            ->where('record_status', 'Cancelled')->count();
        $orderCompleted    = Order::where('delivery_date', '<', $nowDate)
            ->where('record_status', 'Completed')->count();
        return view('welcome', compact('services', 'todayDelivery', 
        'totalPending', 'orderDeliveryOver',
            'orderCancelled',
            'orderCompleted'));
    }

    public function order()
    {
        $services = Service::all();
        return view('orderform',compact('services'));
    }

    public function getUserByName(){
        if (request()->ajax()) {
            $term             = request()->get('term');
            $businessUsers    =  User::select('id', 'name')
                ->where('name', 'like', "%$term%")->where('id','!=',1)->get();
            $result = array();
            if (!$businessUsers->isEmpty()) {
                foreach ($businessUsers as $businessUser) {
                    $result[] = ['id' => $businessUser->id, 'value' => $businessUser->name];
                }
            } } else {
            return response('Forbidden.', 403);
        }
        return response()->json($result);
    }

    public function getUserBySerial()
    {
        if (request()->ajax()) {
            $term             = request()->get('term');
            $businessUsers    =  User::select('id', 'name', 'serial_number')
                ->where('serial_number', 'like', "%$term%")->where('id','!=',1)->get();
            $result = array();
            if (!$businessUsers->isEmpty()) {
                foreach ($businessUsers as $businessUser) {
                    $result[] = ['id' => $businessUser->id, 'value' => $businessUser->serial_number];
                }
            }
        } else {
            return response('Forbidden.', 403);
        }
        return response()->json($result);
    }


    public function getUserByPhone()
    {
        if (request()->ajax()) {
            $term             = request()->get('term');
            $businessUsers    =  User::select('id', 'name', 'phone_number')
                ->where('phone_number', 'like', "%$term%")->where('id','!=',1)->get();
            $result = array();
            if (!$businessUsers->isEmpty()) {
                foreach ($businessUsers as $businessUser) {
                    $result[] = ['id' => $businessUser->id, 'value' => $businessUser->phone_number];
                }
            }
        } else {
            return response('Forbidden.', 403);
        }
        return response()->json($result);
    }

    public function getUsersFullData()
    {
        if (request()->ajax()) {
            $term           = request()->get('term');
            $data = array('sa.user_id', 'users.name', 'users.serial_number', 'users.phone_number',
            'sa.silayi', 'sa.calor_bane', 'sa.shirt_length', 'sa.bazo','sa.chati','sa.tera','sa.gala','sa.shalwar_pocket','sa.daman','sa.half_loozing', 'sa.gol_bazo','sa.gera','sa.shalwar','sa.pocket','sa.buton','sa.damn',
            'sa.patti','sa.pati','sa.kuf','sa.pancha',
            'o.service_attribute_id', 'o.total_amount', 'o.remaining_amount',  'o.sub_total_amount', 'o.discount', 'o.paid_amount',
            'o.quantity', 'o.record_status', DB::raw('DATE_FORMAT(o.delivery_date, "%d/%m/%Y") as delivery_date'));

            $userQuery = User::query()->select($data)->where('sa.user_id', $term);
            $userQuery->join('serviceattributes AS sa', 'users.id', '=', 'sa.user_id');
            $userQuery->join('customer_order AS o', 'users.id', '=', 'o.user_id');
            $userQuery->where('o.record_status','New');
            $result   = $userQuery->first();
            if($result ==  null || empty($result)){
                $result = array();
            }
        } else {
            return response('Forbidden.', 403);
        }
        return response()->json($result);
    }

    public function placeOrder(Request $request)
    {
       // dd($request->get('phone_number'));

        $rules     = [
            'name'          => 'required',
            'phone_number'  => 'required|numeric',
            'serial_number' => 'required|numeric|unique:users'
            ];

        $messages = [
            'required' => 'The :attribute field is required.',
            'numeric'  =>  'The :attribute should be Numberic.',
            'unique'=> 'The :attribute is already taken by another customer.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $inputUsera = $request->only('name', 'serial_number','phone_number');
        $orderData  = $request->only('delivery_date', 'total_amount', 'remaining_amount','quantity', 'sub_total_amount', 'discount', 'paid_amount');
        //dd($request->all());
        $user = User::firstOrCreate($inputUsera);
        if($user){
            $id = $user->id;
            $service = new ServiceAttribute();
            if($service){
                $serviceData  = $request->only(
        'calor_bane', 'daman', 'gala','half_loozing',
        'service_id', 'user_id', 'silayi', 'shirt_length', 'bazo', 'tera', 'gala', 'gol_bazo',  'kuf', 'gera', 'pati', 'damn',
                'chati', 'pocket','patti', 'buton', 'shalwar', 'shalwar_pocket',
        'pancha');
                $serviceData['user_id']        = $id;
                $serviceData['service_id']     = 1;
                $serviceObj                     = ServiceAttribute::firstOrCreate($serviceData);
                $orderData['user_id']              = $id;
                $orderData['service_attribute_id'] = $serviceObj->id;
                 $orderData['delivery_date'] = str_replace('/', '-', $request->delivery_date);
                $orderData['delivery_date']        = date("Y-m-d", strtotime($orderData['delivery_date']));
                $oldOrder = Order::where('user_id', $orderData['user_id'])->where('record_status', 'New')->first();
                if($oldOrder){
                    $oldOrder->record_status = 'Completed';
                    $oldOrder->save();
                }
                $order    = Order::create($orderData); 
            }
            return \Redirect::back()->with('message', '.ریکارڈ کامیابی کے ساتھ محفوظ ہوگیا');
        }
        
    }

    public function updateOrder (Request $request)
    { 
        $inputUsera = $request->only('name', 'serial_number', 'phone_number');
        $user_id    = $request->get('user_id');
        $orderData  = $request->only('delivery_date', 'total_amount', 'remaining_amount', 'quantity', 'sub_total_amount','discount', 'paid_amount');
        $user       = User::updateOrCreate(array('id'=>$user_id),$inputUsera);
        if ($user) {
            $id               = $user->id;
            $service          = new ServiceAttribute();
            if ($service) {
                $serviceData  = $request->only('calor_bane', 'daman', 'gala','half_loozing',
        'service_id', 'user_id', 'silayi', 'shirt_length', 'bazo', 'tera', 'calor_length', 'gol_bazo', 'kuf', 'gera', 'pati', 'damn',
            'chati', 'pocket', 'patti', 'buton', 'shalwar', 'shalwar_pocket','pancha');
                $serviceData['user_id']     = $id;
                $serviceData['service_id']  = 1;
                $service_attribute_id       = $request->get('service_attribute_id'); 
                $serviceObj                 = ServiceAttribute::where("id", $service_attribute_id)->where('user_id', $id)->update($serviceData);
                
                $orderData['user_id']       = $id;
                $orderData['service_attribute_id'] = $service_attribute_id;
                $orderData['delivery_date']        = str_replace('/', '-', $request->delivery_date);
                $orderData['delivery_date']        = date("Y-m-d", strtotime($orderData['delivery_date']));

                $oldOrder = Order::where('user_id', $orderData['user_id'])->where('record_status', 'New')->update($orderData);
            }
            return \Redirect::back()->with('message', '.ریکارڈ کامیابی کے ساتھ اپ ڈیٹ ہوا');
        }
    }

}
