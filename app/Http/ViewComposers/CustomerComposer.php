<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

use App\User;
use App\Models\ServiceAttribute;
use App\Models\Service;
use App\Models\Order;
use DB;

class CustomerComposer
{

    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
         
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = auth()->user();
        $view->with('user', $user);
        $view->with('naam', 'Ismail shah');
    }
}