{{-- @if (session('status'))
    
<div class="alert alert-success" role="alert">
     {{ session('status') }}
    </div>
    @endif --}} 
@extends('layouts.main')
@push('styleFiles')
<!-- bootstrap-wysiwyg -->
<link href="{{ asset('public/vendors/google-code-prettify/bin/prettify.min.css')}}" rel="stylesheet">
	<!-- Select2 -->
	<link href="{{ asset('public/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
		<!-- Switchery -->
		<link href="{{ asset('public/vendors/switchery/dist/switchery.min.css')}}" rel="stylesheet">
			<!-- starrr -->
			<link href="{{ asset('public/vendors/starrr/dist/starrr.css')}}" rel="stylesheet">
				<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
					<style type="text/css" media="screen">
ul.ui-autocomplete {
    z-index: 1100;
}   
    .box-shadow{
        height: 260px !important;
    }

    .bootstrap-select > .dropdown-menu{
      height: 250px;
      max-height: 250px !important;
    }
    ul.ui-menu{
      height: 250px;
      max-height: 250px !important;
      /*overflow: scroll;*/
      overflow-y: scroll;
    }
  </style>
@endpush
@section('content')

<!-- page content -->
<div class="right_col" role="main">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>عناصر فرم</h3>
         </div>
         <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
               <div class="input-group">
                  <input type="text" class="form-control" placeholder="جست و جو برای...">
                  <span class="input-group-btn">
                  <button class="btn btn-default" type="button">برو!</button>
                  </span>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <!-- mutahir code Starts here -->
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
               <div class="x_title">
                  <h2>طرح فرم
                     <small>عناصر فرم های مختلف</small>
                  </h2>
                  <ul class="nav navbar-right panel_toolbox">
                     <li>
                        <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                        </a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">
                        <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                           <li>
                              <a href="#">تنظیمات 1</a>
                           </li>
                           <li>
                              <a href="#">تنظیمات 2</a>
                           </li>
                        </ul>
                     </li>
                     <li>
                        <a class="close-link">
                        <i class="fa fa-close"></i>
                        </a>
                     </li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
                  <br/>
                  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                     <!-- TTT -->
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                           <div class="x_content">
                              <br>
                              <!-- END TT -->
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                 <div class="col-md-4 col-sm-12 col-xs-12">
                                    <label for="name">نام  
                                    <span class="required">*</span> :
                                    </label>
                                    <input type="text" id="name" class="form-control" name="name" required="">
                                 </div>
                                 <div class="col-md-4 col-sm-12 col-xs-12">
                                    <label for="phone_number">فون نمبر  
                                    <span class="required">*</span> :
                                    </label>
                                    <input type="text" id="phone_number" class="form-control" name="phone_number" required="">
                                 </div>
                                 <div class="col-md-4 col-sm-12 col-xs-12">
                                    <label for="serial_number"> سیریل نمبر
                                    <span class="required">*</span> :
                                    </label>
                                    <input type="text" id="serial_number" class="form-control" name="serial_number" value="serial976" required="">
                                 </div>
                                 <div class="clearfix"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     {{-- NAME SERIAL NUBER AND PHONE ENDS HERE --}}
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12" for="shirt_length">قمیض 
                           <span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="text" id="shirt_length" name="shirt_length" required="required" class="form-control col-md-7 col-xs-12">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tera">تيرا
                           <span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="number" id="tera" required="required" class="form-control col-md-7 col-xs-12">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="bazo" class="control-label col-md-3 col-sm-3 col-xs-12">بازو
                           <span
                              class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="bazo" class="form-control col-md-7 col-xs-12" type="number" name="bazo">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="gala" class="control-label col-md-3 col-sm-3 col-xs-12">گلہ 
                           <span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="gala" class="form-control col-md-7 col-xs-12" type="number" name="gala">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="gol_bazo" class="control-label col-md-3 col-sm-3 col-xs-12"> کف / گول بازو
                           <span
                              class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="gol_bazo" class="form-control col-md-7 col-xs-12" type="number" name="gol_bazo">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="chati" class="control-label col-md-3 col-sm-3 col-xs-12">چھاتی
                           <span
                              class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="chati" class="form-control col-md-7 col-xs-12" type="number" name="chati">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="front_chati" class="control-label col-md-3 col-sm-3 col-xs-12">فرنٹ چھاتی 
                           <span
                              class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="front_chati" class="form-control col-md-7 col-xs-12" type="number" name="front_chati">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="half_loozing" class="control-label col-md-3 col-sm-3 col-xs-12">ہالف لوزنگ<
                           <span
                              class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="half_loozing" class="form-control col-md-7 col-xs-12" type="number" name="half_loozing">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="patti" class="control-label col-md-3 col-sm-3 col-xs-12">سامنے پٹی لمبائی
                           <span
                              class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="patti" class="form-control col-md-7 col-xs-12" type="number" name="patti">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="shalwar" class="control-label col-md-3 col-sm-3 col-xs-12">شلوار
                           <span
                              class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="shalwar" class="form-control col-md-7 col-xs-12" type="number" name="shalwar">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="pancha" class="control-label col-md-3 col-sm-3 col-xs-12">پانچہ
                           <span
                              class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="pancha" class="form-control col-md-7 col-xs-12" type="number" name="pancha">
                           </div>
                        </div>
                     </div>
                     {{-- COLUMN 1 ENDS HERE --}}
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">سلائی 
                           <span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <select id="sillay" class="form-control" required>
                                 <option value="سادہ">سادہ</option>
                                 <option value="سادہ ڈبل">سادہ ڈبل</option>
                                 <option value="سادہ ٹرپل">سادہ ٹرپل</option>
                                 <option value="ریشمی ڈبل">ریشمی ڈبل</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">کالر 
                           <span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <select id="kalar" class="form-control" required>
                                 <option value="کالر">کالر</option>
                                 <option value="فل بین">فل بین</option>
                                 <option value="ہالف بین">ہالف بین</option>
                                 <option value="کالر نوک بڑی">کالر نوک بڑی</option>
                                 <option value="کالر نوک چھوٹی">کالر نوک چھوٹی</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">کف
                           <span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <select id="kuf" class="form-control" required>
                                 <option value="کف گول">کف گول</option>
                                 <option value="کف کٹ">کف کٹ</option>
                                 <option value="کف چورس">کف چورس</option>
                                 <option value="کف چونٹ">کف چونٹ</option>
                                 <option value="کف ڈبل بکرم">کف ڈبل بکرم</option>
                                 <option value="کف سنگل">کف سنگل</option>
                                 <option value="کف سادہ ڈبل">کف سادہ ڈبل</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">شلوارجیب
                           <span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <select id="shalwar_pocket" class="form-control" required>
                                 <option value="نہیں ">نہیں </option>
                                 <option value="سادہ پاکٹ">سادہ جیب</option>
                                 <option value="زیب پاکٹ ">زیب جیب </option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">جیب
                           <span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <select id="pocket" class="form-control" required>
                                 <option value="1 سامنے 1 سائیڈ "> 1 سامنے 1 سائیڈ </option>
                                 <option value="1 سامنے  2 سائیڈ"> 1 سامنے  2 سائیڈ</option>
                                 <option value="  2 سائیڈ ">   2 سائیڈ  </option>
                                 <option value=" 1 سائیڈ "> 1 سائیڈ  </option>
                                 <option value="2 سامنے  2 سائیڈ"> 2 سامنے  2 سائیڈ</option>
                                 <option value="2 سامنے  1 سائیڈ"> 2 سامنے  1 سائیڈ </option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">گیرا
                           <span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <select id="gera" class="form-control" required>
                                 <option value="چورس گھیرا">چورس گھیرا</option>
                                 <option value="گول گھیرا">گول گھیرا</option>
                                 <option value="گول گھیرا ڈبل">گول گھیرا ڈبل</option>
                                 <option value="ڈيزائين گ">ڈيزائين گ</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">پٹی 
                           <span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <select id="pati" class="form-control" required>
                                 <option value="ڈبل پٹی">ڈبل پٹی</option>
                                 <option value="سنگل پٹی">سنگل پٹی</option>
                                 <option value="پٹی نوک">پٹی نوک</option>
                                 <option value="پٹی چورس">پٹی چورس</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">سلائی 
                           <span class="required">*</span>
                           </label>
                           <div class="col-md-6 col-sm-6 col-xs-12">
                              <select id="pocket" class="form-control" required>
                                 <option value="1 سامنے 1 سائیڈ "> 1 سامنے 1 سائیڈ </option>
                                 <option value="1 سامنے  2 سائیڈ"> 1 سامنے  2 سائیڈ</option>
                                 <option value="  2 سائیڈ ">   2 سائیڈ  </option>
                                 <option value=" 1 سائیڈ "> 1 سائیڈ  </option>
                                 <option value="2 سامنے  2 سائیڈ"> 2 سامنے  2 سائیڈ</option>
                                 <option value="2 سامنے  1 سائیڈ"> 2 سامنے  1 سائیڈ </option>
                              </select>
                           </div>
                        </div>
                     </div>
                     {{-- COLUMN 2 ENDS HERE --}}
                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                           <button type="submit" class="btn btn-primary">انصراف</button>
                           <button type="submit" class="btn btn-success">ارسال</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- mutahir code ends here -->
   </div>
</div>
<!-- /page content -->
@endsection
@push('blockscripts')

																					<script type="text/javascript" charset="utf-8" defer>
$(document).ready(function ()
  {
    $( "#name").autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "{{route('get.users')}}/",
          dataType: "json",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      }, 
      select: function (event, ui) {
          var selected_value = ui.item.id;
           $.ajax({
            url: "{{route('get.usersfulldata')}}?term="+selected_value,
            method: "get",
            success: function (data) {  
              console.log(data); 
            }
        });
    }
    });

    $( "#serial_number").autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "{{route('get.usersby.serial_number')}}/",
          dataType: "json",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      }, 
      select: function (event, ui) {
    }
    });

    $( "#phone_number").autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "{{route('get.usersby.phone_number')}}/",
          dataType: "json",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      }, 
      select: function (event, ui) {
    }
    });

/*###########################################################*/
  });
</script>
@endpush
@push('scriptsFiles')

																					<!-- bootstrap-wysiwyg -->
																					<script src="{{ asset('public/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')}}"></script>
																					<script src="{{ asset('public/vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
																					<script src="{{ asset('public/vendors/google-code-prettify/src/prettify.js')}}"></script>
																					<!-- jQuery Tags Input -->
																					<script src="{{ asset('public/vendors/jquery.tagsinput/src/jquery.tagsinput.js')}}"></script>
																					<!-- Switchery -->
																					<script src="{{ asset('public/vendors/switchery/dist/switchery.min.js')}}"></script>
																					<!-- Select2 -->
																					<script src="{{ asset('public/vendors/select2/dist/js/select2.full.min.js')}}"></script>
																					<!-- Parsley -->
																					<script src="{{ asset('public/vendors/parsleyjs/dist/parsley.min.js')}}"></script>
																					<script src="{{ asset('public/vendors/parsleyjs/dist/i18n/fa.js')}}"></script>
																					<!-- Autosize -->
																					<script src="{{ asset('public/vendors/autosize/dist/autosize.min.js')}}"></script>
																					<!-- jQuery autocomplete -->
																					<script src="{{ asset('public/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js')}}"></script>
																					<!-- starrr -->
																					<script src="{{ asset('public/vendors/starrr/dist/starrr.js')}}"></script>
																					<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endpush
