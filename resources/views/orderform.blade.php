@extends('layouts.main')
@push('styleFiles')
<!-- bootstrap-wysiwyg -->
<link href="{{ asset('public/vendors/google-code-prettify/bin/prettify.min.css')}}" rel="stylesheet">
<!-- Select2 -->
<link href="{{ asset('public/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
<!-- Switchery -->
<link href="{{ asset('public/vendors/switchery/dist/switchery.min.css')}}" rel="stylesheet">
<!-- starrr -->
<link href="{{ asset('public/vendors/starrr/dist/starrr.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('public/css/jquery-ui.css')}}">
<style type="text/css" media="screen">
ul.ui-autocomplete {
 z-index: 1100;
}   
.box-shadow{
height: 260px !important;
}

.bootstrap-select > .dropdown-menu{
height: 250px;
max-height: 250px !important;
}
ul.ui-menu{
height: 250px;
max-height: 250px !important;
   /*overflow: scroll;*/
overflow-y: scroll;
}
.mainTxt {FONT-SIZE: 16px;COLOR: #000;FONT-FAMILY: "nastliq"; LINE-HEIGHT: 32px;}
 /*urdu menu css start*/ 
</style>
@endpush
@section('content')
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left"style="color:black">
       <h3>گریس فیبرکس اینڈ ٹیلر</h3>
      </div> 
    </div>
  </div>
   <!--<div class="clearfix"></div>
    mutahir code Starts here 
   <div class="row"> -->
  <div class="col-md-12 col-sm-12 col-xs-12">
        <!-- <div class="x_panel"> -->
    <div class="x_content">
      <br/> 
      <div class="x_panel">
        <div class="x_content">

        @if (session('message'))
          <div class="alert alert-success" role="alert">
           {{ session('message') }}
          </div>
        @endif
@if(count($errors) > 0)
  <div class="alert alert-danger" dir="ltr">
    <h4> {{__('Please Fix below Errors')}}<h4/><br>
    <ul>
      @foreach($errors->all() as $error)
      <li><h4>{{ $error }}<h4/></li>
      @endforeach
    </ul>
  </div>
@endif
<form id="demo-form2" accept-charset="utf-8" data-parsley-validate class="form-horizontal form-label-left" method="post" action="{{route('place.order')}}">
<!-- TTT -->
@csrf
<input type="hidden" value="" id="user_id" name="user_id">
<input type="hidden" value="" id="service_attribute_id" name="service_attribute_id">
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
<div class="title_left" style="color:green">
                <h5>براہ کرم ان تینوں فیلڈز کے ذریعہ موجودہ ریکارڈز تلاش کریں۔
                </h5>
              </div>
      
              <br>
        <!-- END TT -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="name" style="color:black">نام  
                <span class="required"></span> :
                </label>
        
                <input type="text" id="name" class="form-control"  name="name" value="{{old('name')}}" required="required" autocomplete="off" dir="ltr">
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                  <label for="phone_number" style="color:black">فون نمبر  
                  <span class="required"></span> :
                  </label> 
                  <input type="text" id="phone_number" class="form-control" name="phone_number" value="{{old('phone_number')}}" autocomplete="off" data-inputmask="'mask' : '99999999999'" required="required" dir="ltr">                        
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                  <label for="serial_number" style="color:black"> سیریل نمبر
                  <span class="required"></span> :
                  </label>
                  <input type="number" id="serial_number" class="form-control" name="serial_number"  value="{{old('serial_number')}}" autocomplete="off" required="required">
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>

     {{---------- NAME SERIAL NUMBER AND PHONE ENDS HERE ----------}}
                 
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="title_left" style="color:green">
       <h5>براہ کرم یہاں صارفین کی پیمائش کی تفصیل درج کریں۔</h5>
          </div>
          <div class="x_content">
          <br/>
               <!-- END TT -->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="shirt_length" style="color:black">قمیص لمبائی  
                <span class="required"></span> :
                </label>
              
                <input type="number" id="shirt_length" class="form-control"  name="shirt_length" required="required" autocomplete="off">
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="tera" style="color:black">تيرا  
                <span class="required"></span> :
                </label> 
                <input type="number" id="tera" class="form-control" name="tera"  autocomplete="off" required="required">            
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="bazo" style="color:black"> بازو
                <span class="required"></span> :
                </label>
                <input type="number" id="bazo" class="form-control" name="bazo" value=""  autocomplete="off" required="required">
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="gala" style="color:black"> گلہ
                <span class="required"></span> :
                </label>
                <input type="number" id="gala" class="form-control" name="gala" value=""  autocomplete="off" required="required">
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="chati" style="color:black"> چھاتی
                <span class="required"></span> :
                </label>
                <input type="number" id="chati" class="form-control" name="chati" value=""  autocomplete="off" required="required">
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="half_loozing" style="color:black"> ہالف لوزنگ
                <span class="required"></span> :
                </label>
                <input type="number" id="half_loozing" class="form-control" name="half_loozing" value=""  autocomplete="off" required="required">
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="daman" style="color:black"> دامن
                <span class="required"></span> :
                </label>
                <input type="number" id="daman" class="form-control" name="daman" value=""  autocomplete="off" required="required">
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="shalwar" style="color:black"> شلوار
                <span class="required"></span> :
                </label>
                <input type="number" id="shalwar" class="form-control" name="shalwar" value=""  autocomplete="off" required="required">
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="pancha" style="color:black"> پانچہ
                <span class="required"></span> :
                </label>
                <input type="number" id="pancha" class="form-control" name="pancha" value=""  autocomplete="off" required="required">
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="gol_bazo" style="color:black"> کف / بازو
                <span class="required"></span> :
                </label>
                <input type="number" id="gol_bazo" class="form-control" name="gol_bazo" value=""  autocomplete="off" required="required">
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="patti" style="color:black"> پٹی لمبائی
                <span class="required"></span> :
                </label>
                <input type="number" id="patti" class="form-control" name="patti" value=""  autocomplete="off" required="required">
              </div>
            </div>
          </div>
        </div>

         {{---------- optional values starts here ----------}}

        <div class="x_panel">
          <div class="title_left" style="color:green">
            <h5>برائے کرم کسٹمر کے اختیاری ڈیزائن کی تفصیل منتخب کریں۔
            </h5>
          </div>
          <br/>
            <div class="x_content">
              <div class="col-md-4 col-sm-12 col-xs-12">
                <label for="silayi" style="color:black"> سلائی
                <span class="required"></span> :
                </label>
                <select id="silayi" name="silayi" class="form-control" required>
                    <option value="سنگل">سنگل</option>
                    <option value="سادہ ڈبل">سادہ ڈبل</option>
                    <option value="سادہ ٹرپل">سادہ ٹرپل</option>
                    <option value="ریشمی ڈبل">ریشمی ڈبل</option>
                </select>             
              </div>                           
            <div class="col-md-4 col-sm-12 col-xs-12">
              <label for="calor_bane" style="color:black"> کالر
              <span class="required"></span> :
              </label>
              <select id="calor_bane" name="calor_bane" class="form-control" required>
                <option value="کالر">کالر</option>
                <option value="فل بین">فل بین</option>
                <option value="ہالف بین">ہالف بین</option>
              </select>
            </div>
                           
            <div class="col-md-4 col-sm-12 col-xs-12">
              <label for="damn" style="color:black"> دامن
              <span class="required"></span> :
              </label>
              <select id="damn" name="damn" class="form-control" required>
                <option value="گول">گول</option>
                <option value="چورس">چورس</option>
              </select>
            </div>
                           
            <div class="col-md-4 col-sm-12 col-xs-12">
              <label for="kuf" style="color:black"> کف
              <span class="required"></span> :
              </label>
              <select id="kuf" name="kuf" class="form-control" required>
                <option value="کف گول">بازو گول</option>
                <option value="کف چورس">کف چورس</option>
                <option value="کف ڈبل بکرم">کف ڈبل بکرم</option>
                <option value="کف سادہ ڈبل">کف سادہ ڈبل</option>
              </select>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
              <label for="shalwar_pocket" style="color:black"> شلوارجیب
              <span class="required"></span> :
              </label>
              <select id="shalwar_pocket" name="shalwar_pocket" class="form-control" required>
                <option value="نہیں">نہیں</option>
                <option value="زیب جیب">زیب جیب</option>
              </select>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
              <label for="pocket" style="color:black"> جیب
              <span class="required"></span> :
              </label>
              <select id="pocket" name="pocket" class="form-control" required>
                <option value="1 سامنے1 سائیڈ">1 سامنے1 سائیڈ</option>
                <option value="1 سامنے2 سائیڈ">1 سامنے2 سائیڈ</option>
                <option value="2 سامنے2 سائیڈ">2 سامنے2 سائیڈ</option>
                <option value="2 سامنے1 سائیڈ">2 سامنے1 سائیڈ</option>
              </select>
            </div>
          <!--<div class="col-md-4 col-sm-12 col-xs-12">
            <label for="gera"> گیرا
              <span class="required">*</span> :
            </label>
            <select id="gera" name="gera" class="form-control"  required>
              <option value="چورس گھیرا">چورس گھیرا</option>
              <option value="گول گھیرا">گول گھیرا</option>
            </select>
          </div>-->
          <div class="col-md-4 col-sm-12 col-xs-12">
            <label for="buton" style="color:black"> بٹن
            <span class="required"></span> :
            </label>
            <select id="buton" name="buton" class="form-control"  required>
              <option value="عام">عام</option>
              <option value="سٹیل">سٹیل</option>
            </select>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12">
            <label for="pati" style="color:black"> پٹی
              <span class="required"></span> :
            </label>
            <select id="pati" name="pati" class="form-control"  required>
              <option value="ڈبل پٹی">ڈبل پٹی</option>
              <option value="سنگل پٹی">سنگل پٹی</option>
            </select>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      </div>
    </div>

    {{---------- optional values end here ----------}}

     {{---------- payment detail start here ----------}}
                 
    <div class="col-md-12 col-sm-12 col-xs-12 gr-tailor">
      <div class="x_panel">
        <div class="title_left" style="color:green">
          <h5>براہ کرم یہاں صارفین کی ادائیگی کی تفصیل درج کریں۔</h5>
        </div>
        <div class="x_content">
        <br>
               <!-- END TT -->
          <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">  
              <label style="color:black" for="single_cal2">Delivery Date<span class="required"> </span> 
              </label> 
              <fieldset>
                <div class="control-group">
                  <div class="controls">
                    <div class="xdisplay_inputx form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left"
                               id="single_cal2" name="delivery_date"
                               aria-describedby="inputSuccess2Status2" autocomplete="off">
                        <span class="fa fa-calendar-o form-control-feedback left"
                              aria-hidden="true"></span>
                        <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                    </div>
                  </div>
              </div>
            </fieldset>         
          </div>
          <div class="col-md-3 col-sm-12 col-xs-12">  
            <label style="color:black" for="item_price">Item Price
            <span class="required"></span> 
            </label>
            <input type="number" name="item_price" id="item_price" value="1000" class="form-control"  autocomplete="off">
          </div>

          <div class="col-md-3 col-sm-12 col-xs-12">
            <label style="color:black" for="quantity">Quantity
            <span class="required"></span> 
            </label>
            <input type="number" id="quantity" value="1" class="form-control" name="quantity"  autocomplete="off">
          </div>
          <div class="col-md-3 col-sm-12 col-xs-12">
            <label style="color:black" for="total_amount">Total Amount<span class="required"></span> 
            </label>
            <input type="number" id="total_amount" value="1000" class="form-control" name="total_amount" readonly>
          </div>
        </div>
      <!-- END TT -->
        <div class="row">
          <div class="col-md-3 col-sm-12 col-xs-12"> 
            <label style="color:black" for="paid_amount">Paid Amount
            <span class="required"></span> 
            </label>
            <input type="number" id="paid_amount" class="form-control" name="paid_amount" value="0"  autocomplete="off">
          </div>
          <div class="col-md-3 col-sm-12 col-xs-12">
            <label style="color:black" for="remaining_amount">Remaining Amount<span class="required"></span> </label>
            <input type="number" id="remaining_amount" class="form-control" name="remaining_amount" value="" readonly >
          </div>
          <div class="col-md-3 col-sm-12 col-xs-12">
            <label style="color:black" for="discount">Discount<span class="required"></span> 
            </label>
            <input type="number" id="discount" class="form-control" name="discount" value="0" autocomplete="off">
          </div>
          <div class="col-md-3 col-sm-12 col-xs-12">
            <label style="color:black" for="sub_total_amount">Total To Pay<span class="required"></span> 
            </label>
            <input type="number" id="sub_total_amount" class="form-control" name="sub_total_amount" readonly >
          </div>
        </div>
      </div>
    </div>

    <div class="ln_solid"></div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="insert">
          <button type="submit" class="btn btn-primary"  id="insertOrder">New</button>
          <button class="btn btn-info printOrder"  id="savePrintOrder" >Save & Print</button>
          <button class="btn btn-primary" id="resetForm">Reset</button>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="update"  style="display:none">
          <button type="submit" class="btn btn-success" title="تازھ کرنا" id="updateOrder">Update</button>
          <button class="btn btn-info" id="updatePrintOrder" >Update And Print</button>
          <button class="btn btn-warning" id="resetForm">Reset</button>
      </div>
    </div>
    </form>
  </div>
</div>
     {{---------- payment detail end here ----------}}

              <!-- mutahir code ends here -->




@endsection
@push('blockscripts')
<script type="text/javascript" charset="utf-8" defer>
$(document).ready(function ()
{
  $('#quantity, #item_price').keyup(function() {
     total_amount = calculateTotal();
     $('#total_amount').val(total_amount);
   });
    $(function() {
     $('#quantity, #item_price').on('change', function(e) {
        total_amount = calculateTotal();
        $('#total_amount').val(total_amount);
        });
 });

  $('#discount').keyup(function() {
     amountPaid();
   });

   $('#paid_amount').keyup(function() {
     amountPaid();
   });




function calculateTotal()
{
var quan = $("#quantity").val() != "" ? parseFloat($("#quantity").val()) : 1,  //  Get quantity value
pric = $("#item_price").val() != "" ? parseFloat($("#item_price").val()) : 0;  //  Get price value
total_amount = pric*quan;
 var paid_amount      = $("#paid_amount").val(); 
  var remaining_amount = $("#remaining_amount").val();
  var discount         = $("#discount").val();
  $("#sub_total_amount").val(total_amount-discount);
  var sub_total_amount = $("#sub_total_amount").val();
  $("#remaining_amount").val(sub_total_amount-paid_amount);

return total_amount;
}
function amountPaid(){
  var total_amount     = $("#total_amount").val();
  var paid_amount      = $("#paid_amount").val(); 
  var remaining_amount = $("#remaining_amount").val();
  var discount         = $("#discount").val();
  $("#sub_total_amount").val(total_amount-discount);
  var sub_total_amount = $("#sub_total_amount").val();
  $("#remaining_amount").val(sub_total_amount-paid_amount); 
}    
  var total_amount     = $("#total_amount").val();

  var paid_amount      = $("#paid_amount").val(); 
  var remaining_amount = $("#remaining_amount").val();
  var discount         = $("#discount").val();
  $("#sub_total_amount").val(total_amount-discount);
  var sub_total_amount = $("#sub_total_amount").val();
  $("#remaining_amount").val(sub_total_amount-paid_amount);

function updateForm(selected_value){
   $.ajax({
         url: "{{route('get.usersfulldata')}}?term="+selected_value,
         method: "get",
         success: function (data) {
            if(Object.keys(data).length >0){
               $("#name").val(data.name);
               $("#user_id").val(data.user_id);
               $("#phone_number").val(data.phone_number);
               $("#serial_number").val(data.serial_number);
               $("#shirt_length").val(data.shirt_length);
               $("#patti").val(data.patti);
               $("#quantity").val(data.quantity);
               $("#single_cal2").val(data.delivery_date);
               $("#remaining_amount").val(data.remaining_amount);
               $("#total_amount").val(data.total_amount);    
               $("#sub_total_amount").val(data.sub_total_amount);
               $("#discount").val(data.discount); 
               $("#paid_amount").val(data.paid_amount);                                     
               $("#tera").val(data.tera);
               $("#gala").val(data.gala);
               $("#chati").val(data.chati);
               $("#daman").val(data.daman);
               $("#shalwar").val(data.shalwar);
               $("#pancha").val(data.pancha);
               $("#half_loozing").val(data.half_loozing);
               $("#gol_bazo").val(data.gol_bazo);
               $('#silayi').val(data.silayi).attr("selected", "selected");
               $("#calor_bane").val(data.calor_bane).attr("selected", "selected");
               $("#damn").val(data.damn).attr("selected", "selected");       
               $("#shalwar_pocket").val(data.shalwar_pocket).attr("selected", "selected");
               $("#pocket").val(data.pocket).attr("selected", "selected");
               $("#kuf").val(data.kuf).attr("selected", "selected");
             //  $("#gera").val(data.gera).attr("selected", "selected");
               $("#pati").val(data.pati).attr("selected", "selected");
               $("#bazo").val(data.bazo).attr("selected", "selected");
               $("#buton").val(data.buton).attr("selected", "selected");
               $("#service_attribute_id").val(data.service_attribute_id);

            }
         }
     });
}
function showButton(reset = false){
   if(reset)
   {
   $("#update").hide();
   $("#insert").show();
   clearForm();
}else
{
   $("#update").show();
   $("#insert").hide();
}

}

function clearForm() { 
   $('#demo-form2').trigger("reset");
   $("#demo-form2")[0].reset();  
   $("select").closest("form").on("reset",function(ev){
   var targetJQForm = $(ev.target);
   setTimeout((function(){
   this.find("select").trigger("change");
   }).bind(targetJQForm),0);
   });
}

$(document).on("click","#resetForm",function(e){
e.preventDefault();
showButton(true);
return false;
});

 $( "#name").autocomplete({
   source: function( request, response ) {
     $.ajax( {
       url: "{{route('get.users')}}/",
       dataType: "json",
       data: {
         term: request.term
       },
       success: function( data ) {
         response( data );
       }
     } );
   }, 
   select: function (event, ui) {
       var selected_value = ui.item.id;
       updateForm(selected_value);
       showButton(false);
       }
 });

 $( "#serial_number").autocomplete({
   source: function( request, response ) {
     $.ajax( {
       url: "{{route('get.usersby.serial_number')}}/",
       dataType: "json",
       data: {
         term: request.term
       },
       success: function( data ) {
         response( data );
       }
     } );
   }, 
   select: function (event, ui) {
      var selected_value = ui.item.id;
       updateForm(selected_value);
       }
 });

 $( "#phone_number").autocomplete({
   source: function( request, response ) {
     $.ajax( {
       url: "{{route('get.usersby.phone_number')}}/",
       dataType: "json",
       data: {
         term: request.term
       },
       success: function( data ) {
         response( data );
       }
     } );
   }, 
   select: function (event, ui) {
      var selected_value = ui.item.id;
       updateForm(selected_value);
       }
 });


 $("#updateOrder").on("click", function(e){
 e.preventDefault();
 $('#demo-form2').attr('action', "{{route('update.order')}}").submit();
});

$("#updatePrintOrder").on("click", function(e){
   e.preventDefault();
   $('#demo-form2').attr('action', "{{route('update.order')}}");
   printForm();
});
$("#savePrintOrder").on("click", function(e){
   e.preventDefault();  
   printForm();
});

function printForm(){
/*$("form").submit(function(e){*/
var data_name          = $("#name").val();
var data_phone_number  =  $("#phone_number").val();
var data_serial_number =   $("#serial_number").val();
var data_shirt_length  =   $("#shirt_length").val();
var data_patti = $("#patti").val();
var data_quantity=   $("#quantity").val();
var data_delivery_date  =               $("#single_cal2").val();
var data_remaining_amount  =   $("#remaining_amount").val();
var data_total_amount =   $("#total_amount").val();    
var data_sub_total_amount=   $("#sub_total_amount").val();
var data_discount=  $("#discount").val(); 
var data_paid_amount=  $("#paid_amount").val();                                     
var data_tera=  $("#tera").val();
var data_gala=  $("#gala").val();
var data_chati=               $("#chati").val();
var data_daman=               $("#daman").val();
var data_shalwar=               $("#shalwar").val();
var data_pancha=               $("#pancha").val();
var data_half_loozing=               $("#half_loozing").val();
var data_gol_bazo  =$("#gol_bazo").val();
var data_silayi= $('#silayi').val();
var data_calor_bane= $("#calor_bane").val();
var data_damn= $("#damn").val();
var data_shalwar_pocket=   $("#shalwar_pocket").val();
var data_pocket= $("#pocket").val();
var data_kuf=$("#kuf").val();
//var data_gera=               $("#gera").val();
var data_pati=               $("#pati").val();
var data_bazo=               $("#bazo").val();
var data_buton=               $("#buton").val(); 
if(data_name =='' || data_phone_number== ''){
   return false;
}
   var table = '<table width="50%" border="0">';
   table += '<thead><tr><th colspan="4">Grace Fabrics & Tailor</th></tr></thead><tbody id="table">';
   table += '<tr><td colspan="4"><hr/><br/><br/></td></tr>';
   table += '<tr><td>Name:</td><td>'+data_name+'</td></tr>';          
   table += '<tr><td>Serial_Number:</td><td>'+data_serial_number+'</td></tr>';
   table += '<tr><td>Quantity:</td><td>'+data_quantity+'</td></tr>';
   table += '<tr><td>Total_Amount:</td><td>'+data_total_amount+'</td></tr>';
   table += '<tr><td>Paid_Amount:</td><td>'+data_paid_amount+'</td></tr>';
   table += '<tr><td>Remaining_Amount:</td><td>'+data_remaining_amount+'</td></tr>';
   var today = new Date();
   var date = today.getMonth()+'/'+(today.getDate())+'/'+today.getFullYear();
   table += '<tr><td>Date:</td><td>'+date+'</td</tr>';
   table += '<tr><td>Delivery_Date:</td><td>'+data_delivery_date+'</td></tr>';
   table += '<tr><td colspan="4"><br/></td></tr>';
   table += '<tr><td colspan="4"><br/<br/></td></tr>'; 
   table += '<tr><td colspan="4">نوٹ : ارجنٹ سوٹ ۳ گھنٹوں میں تیارکریں.</td></tr>';
   table += '<tr><td colspan="4"><br/></td></tr>'; 
   table += "<tr><td colspan='2'></td></tr></tbody></table>"; 
   table += '<br/><br/><br/><br/><br/><br/><br/><br/><br/>';
   table += '<table width="100%" border="0">';
  table += '<thead><tr><th colspan="4">گریس فیبرکس اینڈ ٹیلر </th></tr></thead><tbody id="table">';
  table += '<tr><td colspan="4"><hr/><br/></td></tr>';
  
   table += '<tr><td><b>'+data_name+'</b></td><td>:نام</td></tr>';
   table += '<tr><td><b>'+data_serial_number+'</b></td><td>:نمبر</td></tr>';
   table += '<tr><td>'+data_shirt_length+'</td><td>:قمیص لمبائی:</td></tr>';
   table += '<tr><td>'+data_bazo+'</td><td>:بازو</td></tr>';
   table += '<tr><td>'+data_gala+'</td><td>:گلہ</td></tr>';
   table += '<tr><td>'+data_chati+'</td><td>:چھاتی</td></tr>';
   table += '<tr><td>'+data_half_loozing+'</td><td>:ہالف لوزنگ</td></tr>';
   table += '<tr><td>'+data_daman+'</td><td>:دامن</td></tr>';
   table += '<tr><td>'+data_shalwar+'</td><td>:شلوار</td></tr>';
   table += '<tr><td>'+data_pancha+'</td><td>:پانچہ</td></tr>';
   table += '<tr><td>'+data_gol_bazo+'</td><td>: کف / گول بازو</td></tr>';
   table += '<tr><td>'+data_patti+'</td><td>:سامنے پٹی لمبائی</td></tr>';
   table += '<tr><td>'+data_silayi+'</td><td>:سلائی</td></tr>';
   table += '<tr><td>'+data_calor_bane+'</td><td>:کالر</td></tr>';
   table += '<tr><td>'+data_damn+'</td><td>:دامن</td></tr>';
   table += '<tr><td>'+data_kuf+'</td><td>:کف</td></tr>';
   table += '<tr><td>'+data_shalwar_pocket+'</td><td>:شلوارجیب</td></tr>';
   table += '<tr><td>'+data_pocket+'</td><td>:جیب</td></tr>';
   //table += '<tr><td>'+data_gera+'</td><td>:گیر</td></tr>';
   table += '<tr><td>'+data_buton+'</td><td>:بٹن</td></tr>';
   table += '<tr><td>'+data_pati+'</td><td>:پٹی</td></tr>';
   table += "<tr><td colspan='4'></td></tr></tbody></table>";

w = window.open();
w.document.write(table);
w.document.close(); 
w.focus()
w.print();

//function to call if you want to print
var onPrintFinished=function(printed){
             //https://stackoverflow.com/questions/22363838/submit-form-after-calling-e-preventdefault
$( "#demo-form2" ).unbind('submit').submit();
}
//print command 
onPrintFinished(w.close()); 

}// END FUNCTION OF PRINT AND SAVE.
/*###########################################################*/
});

</script>
@endpush
@push('scriptsFiles')
<!-- bootstrap-wysiwyg -->
<script src="{{ asset('public/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')}}"></script>
<script src="{{ asset('public/vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
<script src="{{ asset('public/vendors/google-code-prettify/src/prettify.js')}}"></script>
<!-- jQuery Tags Input -->
<script src="{{ asset('public/vendors/jquery.tagsinput/src/jquery.tagsinput.js')}}"></script>
<!-- Switchery -->
<script src="{{ asset('public/vendors/switchery/dist/switchery.min.js')}}"></script>
<!-- Select2 -->
<script src="{{ asset('public/vendors/select2/dist/js/select2.full.min.js')}}"></script>
<!-- Parsley -->
<script src="{{ asset('public/vendors/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{ asset('public/vendors/parsleyjs/dist/i18n/fa.js')}}"></script>
<!-- Autosize -->
<script src="{{ asset('public/vendors/autosize/dist/autosize.min.js')}}"></script>
<!-- jQuery autocomplete -->
<script src="{{ asset('public/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js')}}"></script>
<!-- starrr -->
<script src="{{ asset('public/vendors/starrr/dist/starrr.js')}}"></script>
<script src="{{ asset('public/js/jquery-ui.js')}}"></script>
<!-- jquery.inputmask -->
 <script src="{{ asset('public/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js')}}"></script>
@endpush
