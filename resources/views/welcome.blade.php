          {{--  @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif --}}

@extends('layouts.main')
@push('styleFiles')
<!-- bootstrap-wysiwyg -->
<link href="{{ asset('public/vendors/google-code-prettify/bin/prettify.min.css')}}" rel="stylesheet">
<!-- Select2 -->
<link href="{{ asset('public/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
<!-- Switchery -->
<link href="{{ asset('public/vendors/switchery/dist/switchery.min.css')}}" rel="stylesheet">
<!-- starrr -->
<link href="{{ asset('public/vendors/starrr/dist/starrr.css')}}" rel="stylesheet">

@endpush
@section('content')

<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row tile_count">
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Deliveries</span>
            <div class="count">{{$totalPending}}</div> 
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-clock-o"></i>Orders Delivery Over</span>
            <div class="count">{{$orderDeliveryOver}}</div> 
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i>Today Deliveries</span>
            <div class="count green">{{$todayDelivery}}</div> 
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i>Cancelled Deliveries</span>
            <div class="count">{{$orderCancelled}}</div> 
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> مجموعه کلی</span>
            <div class="count">{{$orderCancelled}}</div> 
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i>Completed Orders</span>
            <div class="count">{{$orderCompleted}}</div> 
        </div>
    </div>
    <!-- /top tiles -->

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-6">
                        <h3>Dashboard <small>Orders Reports</small>
                        </h3>
                    </div>
                    <div class="col-md-6">
                        <div id="reportrange" class="pull-left"
                             style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                            <span>اسفند 29, 1394 - فروردین 28, 1395</span> <b class="caret"></b>
                        </div>
                    </div>
                </div>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                    
                </div>
                

                <div class="clearfix"></div>
            </div>
        </div>

    </div>
    <br/>

</div>
<!-- /page content -->
@endsection
@push('blockscripts')
@endpush
@push('scriptsFiles')
 <!-- Chart.js -->
<script src="{{ asset('public/vendors/Chart.js/dist/Chart.min.js')}}"></script>
<!-- jQuery Sparklines -->
<script src="{{ asset('public/vendors/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- gauge.js')}} -->
<script src="{{ asset('public/vendors/gauge.js/dist/gauge.min.js')}}"></script>
<!-- Skycons -->
<script src="{{ asset('public/vendors/skycons/skycons.js')}}"></script>
<!-- Flot -->
<script src="{{ asset('public/vendors/Flot/jquery.flot.js')}}"></script>
<script src="{{ asset('public/vendors/Flot/jquery.flot.pie.js')}}"></script>
<script src="{{ asset('public/vendors/Flot/jquery.flot.time.js')}}"></script>
<script src="{{ asset('public/vendors/Flot/jquery.flot.stack.js')}}"></script>
<script src="{{ asset('public/vendors/Flot/jquery.flot.resize.js')}}"></script>
<!-- Flot plugins -->
<script src="{{ asset('public/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
<script src="{{ asset('public/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
<script src="{{ asset('public/vendors/flot.curvedlines/curvedLines.js')}}"></script>
<!-- DateJS -->
<script src="{{ asset('public/vendors/DateJS/build/production/date.min.js')}}"></script>
<!-- JQVMap -->
<script src="{{ asset('public/vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
<script src="{{ asset('public/vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
<script src="{{ asset('public/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
@endpush