{{-- @if (session('status'))
    <div class="alert alert-success" role="alert">
     {{ session('status') }}
    </div>
    @endif --}} 
@extends('layouts.main')
@push('styleFiles')
 <!-- Datatables -->
 
@endpush
@section('content')
<!-- page content -->
<div class="right_col" role="main">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>گریس فیبرکس اینڈ ٹیلر</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <!-- mutahir code Starts here -->
      <div class="row"> 
          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            <div class="x_content">
                                    
             <table id="dtable" class="table table-striped table-bordered">
        <thead>
            <tr>
             <th>نام</th>
             <th>سیریل نمبر</th>
             <th>ٹیلی فون نمبر</th>
             <th>{{ __('Actions') }}</th>
            </tr>
        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
      </div>
     
<!-- /page content -->


@endsection
@push('blockscripts')
<script type="text/javascript" charset="utf-8" defer>
$(document).ready(function ()
  {
       $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json'
    });
/*###########################################################*/
   $('#dtable').DataTable({
               processing: true,
               serverSide: true,
               ajax: '{{ url('getdata') }}',
                  columns: [
                      {data: 'name', name: 'name'},
                      {data: 'serial_number', name: 'serial_number'},
                      {data: 'phone_number', name: 'phone_number'},

{ data: 'action', name: 'edit', orderable: false, searchable: false},
                      ]
                      }); 
            
  });
</script>
@endpush
@push('scriptsFiles')

<!-- Datatables -->
 <script src="{{ asset('public/js/jquery.dataTables.min.js')}}"></script>
@endpush
