@extends('services.layouts')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Service</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('services.index') }}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   <form method="post" action="{{route('services.store')}}">
    @csrf
   
     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                           <div class="x_content">
                              <br>
                              <!-- END TT -->
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                 <div class="col-md-4 col-sm-12 col-xs-12">
                                     <strong>Name:</strong>
                                    <!-- class="form-control unicode mainTxt" -->
                                    <input type="text" id="name" class="form-control"  name="name"  autocomplete="off">
                                 </div>
                                     <strong>Status:</strong>
                             <div class="form-group">
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select id="status" name="status" class="form-control" required>
                                   <option value="Active">Active </option>
                                   <option value="Inactive">Inactive</option>
                                  </select>
                                </div>
                               </div>   
                                 <div class="clearfix"></div>
                              </div>
                           </div>
                        </div>
                     </div>   
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            
        </div>
       
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
@endsection