-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 09, 2020 at 06:40 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tailor`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer_order`
--

DROP TABLE IF EXISTS `customer_order`;
CREATE TABLE IF NOT EXISTS `customer_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service_attribute_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL DEFAULT 1,
  `order_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `delivery_date` date NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `paid_amount` decimal(10,2) DEFAULT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `sub_total_amount` decimal(10,0) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `remaining_amount` decimal(10,0) DEFAULT NULL,
  `status` enum('Cancelled','Delivered','Pending','Postponed','Completed') NOT NULL DEFAULT 'Pending',
  `record_status` enum('New','Completed') NOT NULL DEFAULT 'New',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `service_attribute_id` (`service_attribute_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_order`
--

INSERT INTO `customer_order` (`id`, `user_id`, `service_attribute_id`, `quantity`, `order_date`, `delivery_date`, `total_amount`, `paid_amount`, `discount`, `sub_total_amount`, `details`, `remaining_amount`, `status`, `record_status`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 1, '2020-06-30 09:28:40', '1970-01-01', '1000.00', '500.00', '0', '1000', NULL, '500', 'Pending', 'New', NULL, '2020-06-30 09:28:40'),
(2, 5, 2, 1, '2020-07-03 11:14:40', '2020-07-03', '1000.00', '0.00', '0', '1000', NULL, '1000', 'Pending', 'New', NULL, '2020-07-03 11:14:40'),
(3, 6, 3, 2, '2020-07-05 04:11:06', '1970-01-01', '2000.00', '0.00', '0', '2000', NULL, '2000', 'Pending', 'Completed', NULL, '2020-07-05 04:11:06'),
(4, 7, 4, 3, '2020-07-06 06:36:40', '2020-07-14', '3000.00', '0.00', '500', '2500', NULL, '2500', 'Pending', 'Completed', NULL, '2020-07-06 06:36:40'),
(5, 8, 5, 2, '2020-06-30 17:40:35', '2020-06-30', '2000.00', '0.00', '200', '1800', NULL, '1800', 'Pending', 'Completed', NULL, '2020-06-30 17:40:35'),
(6, 8, 5, 1, '2020-07-07 06:07:17', '2020-07-07', '1000.00', '0.00', '0', '1000', NULL, '1000', 'Pending', 'New', NULL, '2020-07-07 06:07:17'),
(7, 9, 7, 1, '2020-07-05 09:08:59', '2020-08-01', '1000.00', '0.00', '0', '1000', NULL, '1000', 'Pending', 'New', NULL, '2020-07-05 09:08:59'),
(8, 6, 8, 2, '2020-07-05 04:11:06', '1970-01-01', '2000.00', '0.00', '0', '2000', NULL, '2000', 'Pending', 'New', NULL, NULL),
(9, 7, 9, 3, '2020-07-06 06:36:40', '2020-07-14', '3000.00', '0.00', '500', '2500', NULL, '2500', 'Pending', 'New', NULL, NULL),
(10, 12, 10, 1, '2020-07-09 15:42:26', '2020-07-15', '900.00', '0.00', '100', '800', NULL, '800', 'Pending', 'Completed', NULL, '2020-07-09 15:42:26'),
(11, 12, 10, 1, '2020-07-09 15:42:26', '2020-07-15', '900.00', '0.00', '100', '800', NULL, '800', 'Pending', 'New', NULL, NULL),
(12, 13, 11, 1, '2020-07-09 16:06:17', '2020-07-09', '900.00', '0.00', '100', '800', NULL, '800', 'Pending', 'New', NULL, NULL),
(13, 14, 12, 2, '2020-07-09 16:17:56', '2020-07-15', '1800.00', '0.00', '0', '1800', NULL, '1800', 'Pending', 'Completed', NULL, '2020-07-09 16:17:56'),
(14, 14, 12, 2, '2020-07-09 16:18:32', '2020-07-15', '1800.00', '0.00', '0', '1800', NULL, '1800', 'Pending', 'Completed', NULL, '2020-07-09 16:18:32'),
(15, 14, 12, 2, '2020-07-09 16:18:32', '2020-07-15', '1800.00', '0.00', '0', '1800', NULL, '1800', 'Pending', 'New', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_05_15_202644_create_service_attributes_table', 1),
(5, '2020_06_16_123956_create_posts_table', 1),
(6, '2020_06_16_154022_create_services_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `serviceattributes`
--

DROP TABLE IF EXISTS `serviceattributes`;
CREATE TABLE IF NOT EXISTS `serviceattributes` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `shirt_length` double(8,2) DEFAULT NULL COMMENT 'لمبآیی',
  `tera` double(8,2) DEFAULT NULL COMMENT 'تیرا',
  `chati` double(8,2) DEFAULT NULL COMMENT 'چھاتی',
  `daman` double(8,2) DEFAULT NULL COMMENT 'دامن',
  `bazo` double(8,2) DEFAULT NULL COMMENT 'بازو',
  `gala` double(8,2) DEFAULT NULL COMMENT 'گلہ',
  `shalwar` double(8,2) DEFAULT NULL COMMENT 'شلوار',
  `pancha` double(8,2) DEFAULT NULL COMMENT 'پانچہ',
  `half_loozing` double(8,2) DEFAULT NULL COMMENT 'ہالف لوزنگ',
  `gol_bazo` double(8,2) DEFAULT NULL COMMENT 'گول بازو',
  `patti` double(8,2) DEFAULT NULL COMMENT 'پٹی لمبایی',
  `silayi` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT 'سنگل',
  `calor_bane` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT 'کالر',
  `kuf` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT 'کف',
  `shalwar_pocket` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT 'شلوارجیب',
  `pocket` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT 'جیب',
  `gera` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT 'گیرا',
  `pati` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT 'پٹی',
  `buton` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT 'پٹی',
  `damn` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT 'دامن',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `serviceattributes`
--

INSERT INTO `serviceattributes` (`id`, `user_id`, `service_id`, `shirt_length`, `tera`, `chati`, `daman`, `bazo`, `gala`, `shalwar`, `pancha`, `half_loozing`, `gol_bazo`, `patti`, `silayi`, `calor_bane`, `kuf`, `shalwar_pocket`, `pocket`, `gera`, `pati`, `buton`, `damn`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 43.00, 12.00, 12.00, 12.00, 21.00, 121.00, 12.00, 9.00, 12.00, 121.00, 12.00, 'ریشمی ڈبل', 'ہالف بین', 'کف سادہ ڈبل', 'نہیں', '1 سامنے 1 سائیڈ', 'گول گھیرا', 'پٹی چورس', 'عام', 'چورس', NULL, NULL),
(2, 5, 1, 22.00, 22.00, 33.00, 33.00, 22.00, 33.00, 33.00, 33.00, 33.00, 33.00, 3.00, 'ریشمی ڈبل', 'ہالف بین', 'کف ڈبل بکرم', 'نہیں', '1 سامنے1 سائیڈ', 'گول گھیرا', 'سنگل پٹی', 'سٹیل', 'گول', NULL, NULL),
(3, 6, 1, 22.00, 22.00, 2.00, 2.00, 22.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 'سادہ', 'فل بین', 'کف ڈبل بکرم', 'زیب جیب', '1 سامنے  2 سائیڈ', 'گول گھیرا', 'سنگل پٹی', 'سٹیل', 'چورس', NULL, NULL),
(4, 7, 1, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 'سادہ ڈبل', 'فل بین', 'کف ڈبل بکرم', 'زیب جیب', '1 سامنے1 سائیڈ', 'گول گھیرا', 'سنگل پٹی', 'سٹیل', 'چورس', NULL, NULL),
(5, 8, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ریشمی ڈبل', 'فل بین', 'کف ڈبل بکرم', 'زیب جیب', '1 سامنے2 سائیڈ', 'گول گھیرا', 'سنگل پٹی', 'سٹیل', 'چورس', NULL, NULL),
(6, 8, 1, 11.00, 11.00, 11.00, 11.00, 11.00, 11.00, 11.00, 11.00, 11.00, 11.00, 11.00, 'سادہ ٹرپل', 'ہالف بین', 'کف ڈبل بکرم', 'زیب جیب', '1 سائیڈ', 'گول گھیرا', 'پٹی نوک', 'سٹیل', 'چورس', NULL, NULL),
(7, 9, 1, 22.00, 23.00, 23.00, 23.00, 23.00, 23.00, 23.00, 23.00, 23.00, 23.00, 23.00, 'سادہ ٹرپل', 'کالر', 'کف گول', 'نہیں', '1 سامنے2 سائیڈ', 'گول گھیرا ڈبل', 'سنگل پٹی', 'سٹیل', 'گول', NULL, NULL),
(8, 6, 1, 22.00, 22.00, 2.00, 2.00, 22.00, 2.00, 2.00, 2.00, 2.00, 2.00, 2.00, 'سنگل', 'فل بین', 'کف ڈبل بکرم', 'زیب جیب', '1 سامنے1 سائیڈ', 'گیرا', 'سنگل پٹی', 'سٹیل', 'چورس', NULL, NULL),
(9, 7, 1, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 22.00, 'سادہ ڈبل', 'فل بین', 'کف ڈبل بکرم', 'زیب جیب', '1 سامنے1 سائیڈ', 'گیرا', 'سنگل پٹی', 'سٹیل', 'چورس', NULL, NULL),
(10, 12, 1, 43.00, 18.50, 34.00, 21.00, 24.00, 14.50, 41.00, 8.00, 19.00, 8.50, 13.00, 'سنگل', 'کالر', 'کف چورس', 'زیب جیب', '1 سامنے1 سائیڈ', 'گیرا', 'سنگل پٹی', 'سٹیل', 'چورس', NULL, NULL),
(11, 13, 1, 44.00, 20.00, 40.00, 24.50, 23.50, 16.00, 38.50, 9.00, 23.50, 6.00, 13.50, 'سنگل', 'ہالف بین', 'کف گول', 'نہیں', '1 سامنے2 سائیڈ', 'گیرا', 'ڈبل پٹی', 'عام', 'چورس', NULL, NULL),
(12, 14, 1, 43.50, 19.00, 44.50, 25.00, 23.50, 17.00, 39.00, 8.50, 25.00, 9.50, 13.50, 'سنگل', 'کالر', 'کف چورس', 'نہیں', '1 سامنے2 سائیڈ', 'گیرا', 'سنگل پٹی', 'عام', 'گول', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `sort_order`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Shalwar Qamees', 0, 'Active', '2020-06-30 09:04:14', '2020-06-30 09:04:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'phone_number',
  `serial_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'serial_number',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`) USING HASH
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone_number`, `serial_number`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Fayaz Khan', 'fayaz@gmail.com', '123', '2', NULL, '$2y$10$2Pbarkw7jJMN7Jm1u8jkrO3A2.OQt6JEvKJxLTyAVxaOgn.Ujp2xe', NULL, '2020-06-30 14:06:37', '2020-06-30 14:06:37'),
(6, 'bilal', NULL, '03009003096', '5', NULL, NULL, NULL, '2020-06-30 18:46:29', '2020-06-30 18:46:29'),
(3, 'Mutahir Syed', NULL, '03349021704', '3', NULL, NULL, NULL, '2020-06-30 16:06:02', '2020-06-30 16:06:44'),
(5, 'fahad', NULL, '03499039559', '4', NULL, NULL, NULL, '2020-06-30 16:32:15', '2020-07-01 00:57:06'),
(7, 'hamza', NULL, '03348461911', '1', NULL, NULL, NULL, '2020-07-01 00:37:41', '2020-07-01 00:37:41'),
(8, 'haidar', NULL, '03499039559', '2', NULL, NULL, NULL, '2020-07-01 00:39:00', '2020-07-01 00:39:00'),
(9, 'imran', NULL, '03219003636', '5', NULL, NULL, NULL, '2020-07-01 13:16:19', '2020-07-01 14:57:19'),
(12, 'fayaz', NULL, '03139194575', '00001', NULL, NULL, NULL, '2020-07-09 10:38:55', '2020-07-09 10:38:55'),
(13, 'shah fahad', NULL, '03138352198', '00002', NULL, NULL, NULL, '2020-07-09 11:06:17', '2020-07-09 11:06:17'),
(14, 'nasim khan', NULL, '03159179175', '00003', NULL, NULL, NULL, '2020-07-09 11:17:12', '2020-07-09 11:17:12');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
