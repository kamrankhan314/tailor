<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/order', 'HomeController@order')->name('order');
Route::get('/getusers', 'HomeController@getUserByName')->name('get.users');
Route::get('/getusersbysrnumber', 'HomeController@getUserBySerial')->name('get.usersby.serial_number');
Route::get('/getusersbyphone', 'HomeController@getUserByPhone')->name('get.usersby.phone_number');


Route::get('/getusersfulldata', 'HomeController@getUsersFullData')->name('get.usersfulldata');
Route::post('/placeorder', 'HomeController@placeOrder')->name('place.order');
Route::post('/updateorder', 'HomeController@updateOrder')->name('update.order');
Route::get('/getdata', 'UserController@getData');
Route::resource('users', 'UserController');
//Route::resource('services', 'ServiceController');
Route::get('/services', 'ServiceController@index')->name('services.index');
Route::get('services/create', 'ServiceController@create')->name('services.create');
Route::get('/services/{service}', 'ServiceController@show')->name('services.show');
Route::get('/services/{service}/edit', 'ServiceController@edit')->name('services.edit');
Route::put('/services/{service}', 'ServiceController@update')->name('services.update');
Route::delete('/services/{service}', 'ServiceController@destroy')->name('services.destroy');
Route::post('/services', 'ServiceController@store')->name('services.store');